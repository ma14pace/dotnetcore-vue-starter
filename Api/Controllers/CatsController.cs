using Api.PvpSecurity;
using Api.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Api.Controllers;

/// <summary>
/// retrieve/insert cat into db
/// </summary>
[ApiController]
[Route("cat/[action]")]
public class CatsController : ControllerBase
{
    private DbContextCscStarter _ctx;
    private readonly ILogger<CatsController> _logger;

    public CatsController(ILogger<CatsController> logger, DbContextCscStarter ctx)
    {
        _logger = logger;
        _logger.LogInformation("Constructor CatsController called");
        _ctx = ctx;
    }

    [HttpPost(), ActionName(""), PvpRole("CatAdmin")]
    public async Task<Cat?> CatInsert([FromBody] Cat cat)
    {
        await _ctx.Cats.AddAsync(cat);
        await _ctx.SaveChangesAsync();

        return cat;
    }

    [HttpPut(), ActionName(""), PvpRole("CatAdmin")]
    public Task<Cat?> CatUpdate([FromBody] Cat cat)
    {
        throw new NotImplementedException();
    }

    [HttpGet()]
    [ActionName(nameof(All)), PvpRole("CatAdmin")]
    public async Task<IEnumerable<Cat>> All()
    {
        return await _ctx.Cats.ToListAsync();
    }

    [HttpGet("{id}")]
    [ActionName(nameof(FindBy))]
    // [HttpGet("/cat//{id}")]
    public async Task<Cat?> FindBy([FromRoute] Guid id)
    {
        return await _ctx.Cats.FirstOrDefaultAsync(x => x.Id == id);
    }
}
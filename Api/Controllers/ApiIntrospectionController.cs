using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [ApiController]
    [Route("/[action]")]
    public class ApiIntrospectionController : ControllerBase
    {
        private readonly ILogger<ApiIntrospectionController> _logger;
        public ApiIntrospectionController(ILogger<ApiIntrospectionController> logger)
        {
            _logger = logger;
            _logger.LogInformation("Constructor VersionController called");
        }

        [HttpGet(), ActionName("whoiam")]
        public Dictionary<string, string> WhoIAm()
        {
            Dictionary<string, string> requestHeaders = new();
            Request.Headers.ToList().ForEach(x => requestHeaders.Add(x.Key, x.Value));
            return requestHeaders;
        }
    }
}
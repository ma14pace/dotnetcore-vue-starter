using Api.AppSettings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Api.Controllers
{
    [ApiController]
    [Route("/[action]")]
    public class ApiInfoController : ControllerBase
    {
        private readonly ILogger<ApiInfoController> _logger;
        private readonly IWebHostEnvironment _env;
        private readonly TestSettings _testSettings;
        public ApiInfoController(ILogger<ApiInfoController> logger, IWebHostEnvironment env, IOptions<TestSettings> optionsTestSettings)
        {
            _logger = logger;
            _logger.LogInformation("Constructor VersionController called");
            _env = env;
            _testSettings = optionsTestSettings.Value;
        }

        /// <summary>
        /// gibt infos über die Version selbst
        /// </summary>
        /// <returns>VersionNumber</returns>
        [HttpGet(), ActionName("version")]
        public object GetVersion()
        {
            return new { value = Environment.GetEnvironmentVariable("APP_VERSION") ?? "ND" };
        }

        /// <summary>
        /// dev, test or produduction enviromnet
        /// </summary>
        /// <returns></returns>
        [HttpGet(), ActionName("zone")]
        public string GetZone()
        {
            return _env.EnvironmentName;
        }
    }
}
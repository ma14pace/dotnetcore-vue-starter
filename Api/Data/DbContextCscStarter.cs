﻿using Microsoft.EntityFrameworkCore;

namespace Api.Data;

/// <summary>
/// to access the database via EF Core
/// </summary>
public partial class DbContextCscStarter : DbContext
{
    public DbContextCscStarter()
    {
    }

    public DbContextCscStarter(DbContextOptions<DbContextCscStarter> options)
        : base(options)
    {
    }

    // Tables
    public virtual DbSet<Cat> Cats { get; set; } = null!;

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseLowerCaseNamingConvention();
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Cat>(entity =>
        {
            // examples

            // entity.HasNoKey();    

            //entity.ToTable("cat", "cscstarter");

            //entity.Property(e => e.Created)
            //    .HasColumnType("timestamp without time zone")
            //    .HasColumnName("created")
            //    .HasDefaultValueSql("CURRENT_TIMESTAMP");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
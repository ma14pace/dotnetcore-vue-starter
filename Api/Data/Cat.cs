﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Data;

/// <summary>
/// Table Cat from the database
/// </summary>
[Table("cat")]
public partial class Cat
{
    /// <summary>
    /// unique Iíd from cat
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Name of the cat
    /// </summary>
    public string Name { get; set; } = null!;

    /// <summary>
    /// Description of the cat
    /// </summary>
    public string Description { get; set; } = null!;

    /// <summary>
    /// Name of image from cat
    /// </summary>
    [Column("image_name")]
    public string? ImageName { get; set; }

    /// <summary>
    /// timestamp of creation 
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTime? Created { get; set; }

    /// <summary>
    /// timestamp of modification 
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTime? Updated { get; set; }
}

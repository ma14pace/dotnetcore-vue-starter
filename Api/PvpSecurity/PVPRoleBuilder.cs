﻿using System.Collections.ObjectModel;

namespace Api.PvpSecurity;

/// <summary>
/// Baut <see cref="PVPRole"/>
/// </summary>
internal class PVPRoleBuilder
{
    /// <summary>
    /// Rollenname
    /// </summary>
    public string RoleName { get; set; }

    /// <summary>
    /// Parameter
    /// </summary>
    public Dictionary<string, string> Parameters { get; set; } = new Dictionary<string, string>();

    /// <summary>
    /// <see cref="PVPRole"/> erstellen
    /// </summary>
    public PVPRole BuildPVPRole()
    {
        return new PVPRole { RoleName = RoleName, Parameters = new ReadOnlyDictionary<string, string>(Parameters) };
    }
}
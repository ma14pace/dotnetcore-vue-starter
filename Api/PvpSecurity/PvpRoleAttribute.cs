﻿namespace Api.PvpSecurity;

using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Linq;
using System.Configuration;

/// <summary>
/// Stellt sicher, dass bestimmte PVP-Rollen vorhanden sind
/// </summary>
[AttributeUsage(AttributeTargets.All)]
public sealed class PvpRoleAttribute : Attribute, IActionFilter
{
    private readonly string[] _rollen;

    /// <summary>
    /// Vorrausgesetzte Rollen (alle werden benötigt!)
    /// </summary>
    public string[] Rollen
    {
        get
        {
            if (_rollen == null)
                return Array.Empty<string>();
            return _rollen;
        }
    }

    /// <inheritdoc/>
    public PvpRoleAttribute() { }

    /// <summary>
    /// Konstruktor
    /// </summary>
    /// <param name="rollen">Vorrausgesetzte Rollen (alle werden benötigt!)</param>
    public PvpRoleAttribute(params string[] rollen)
    {
        _rollen = rollen;
    }

    record PvpSecuritySetting
    {
        public bool Disabled { get; set; }
    }

    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"/>
    /// <exception cref="UnauthorizedAccessException"/>
    public void OnActionExecuting(ActionExecutingContext context)
    {
        // lese Configuration, wenn vorhanden
        var configuration = context.HttpContext.RequestServices.GetService<IConfiguration>();
        if (configuration == null)
            throw new ArgumentNullException(); 

        var positionOptions = new PvpSecuritySetting();
        configuration.GetSection("PvpSecurity").Bind(positionOptions);

        // umgebunginfos holen
        var env = context.HttpContext.RequestServices.GetService<IHostEnvironment>();

        // Ausnahme wenn gewünscht und nur nicht in Prod
        if (env.IsDevelopment() && positionOptions.Disabled)
            return;

        if (context == null)
            throw new ArgumentNullException(nameof(context));

        // rollen überprüfen
        var benötigteRollen = Rollen;
        var currentRoles = context.HttpContext.Request.Headers.GetRolesFromHeader();
        if (currentRoles == null)
        {
            context.Result = new ErrorResult("Keine Rollen übergeben",
                System.Net.HttpStatusCode.Unauthorized);
            return;
        }

        foreach (var benötigteRolle in benötigteRollen)
        {
            if (!currentRoles.Any(role => role.RoleName.Equals(benötigteRolle, StringComparison.OrdinalIgnoreCase)))
            {
                context.Result = new ErrorResult($"Rolle nicht übergeben: {benötigteRolle}",
                    System.Net.HttpStatusCode.Unauthorized);
                return;
            }
        }
    }

    /// <inheritdoc/>
    public void OnActionExecuted(ActionExecutedContext context) { }
}
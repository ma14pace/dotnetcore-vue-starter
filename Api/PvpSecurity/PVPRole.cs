﻿using System.Text;

namespace Api.PvpSecurity;

/// <summary>
/// Rolle im Portalverbund
/// </summary>
public record PVPRole
{
    /// <summary>
    /// Rollenname
    /// </summary>
    public string RoleName { get; set; }

    /// <summary>
    /// Parameter
    /// </summary>
    public IReadOnlyDictionary<string, string> Parameters { get; set; }

    /// <inheritdoc />
    public override string ToString()
    {
        var builder = new StringBuilder();
        builder.Append(RoleName);
        if (Parameters != null)
        {
            builder.Append('(');
            bool firstParameter = true;
            foreach (var parameter in Parameters)
            {
                if (firstParameter)
                    firstParameter = false;
                else
                    builder.Append(", ");
                builder.Append($"{parameter.Key}={parameter.Value}");
            }
            builder.Append(')');
        }
        return builder.ToString();
    }
}
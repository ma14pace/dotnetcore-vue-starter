﻿using Microsoft.AspNetCore.Mvc;
using System.Text;

namespace Api.PvpSecurity;

/// <summary>
/// Fehlerobject
/// </summary>
public class ErrorResult : IActionResult
{
    private readonly int? _statusCode;
    private readonly string _message;

    /// <summary>
    /// Konstruktor mit Exceptions
    /// </summary>
    /// <exception cref="ArgumentNullException"/>
    public ErrorResult(Exception exep)
    {
        _message = exep.Message ?? throw new ArgumentNullException(nameof(exep));
    }

    internal ErrorResult(string message, System.Net.HttpStatusCode statusCode)
    {
        _message = message;
        _statusCode = (int)statusCode;
    }

    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"/>
    public async Task ExecuteResultAsync(ActionContext context)
    {
        var response = context?.HttpContext?.Response;
        if (response == null)
            throw new ArgumentNullException(nameof(context.HttpContext.Response));

        response.ContentType = "text/html; charset=UTF-8";
        response.StatusCode = _statusCode == null ? 500 : _statusCode.Value;

        await response.WriteAsync(_message ?? "Server Error", Encoding.UTF8).ConfigureAwait(false);
    }
}
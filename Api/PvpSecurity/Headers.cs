﻿using System.Diagnostics.CodeAnalysis;

namespace Api.PvpSecurity;

/// <summary>
/// Sammlung an PVP-Headernamen
/// </summary>
[SuppressMessage("Naming", "CA1724: Type names should not match namespaces", Justification =
    "Ich würde sagen akzeptabel, da statische Klasse für Konstanten und Verwendung ähnlich wie Namespace")]
public static class PvpHeaders
{
    /// <summary>
    /// Beschreibt die PVP-Rollen, die den Benutzern von der zuständigen vom Participantbeauftragen
    /// Rechteverwaltung zugeordnet wurden.<br/>
    /// Die möglichen Rollen einer Anwendung werden durch gvApplicationRight Objekte beschrieben.
    /// </summary>
    public const string ROLES = "X-PVP-ROLES";

    /// <summary>
    /// This attribute contains computer system login names associated with the object.
    /// Each name is one value of this multi-valued attribute.
    /// </summary>
    public const string USERID = "X-PVP-USERID";

    /// <summary>
    /// Bereichsspezifisches Personenkennzeichen (bPK) inklusiveBereichsangabe.
    /// Dabei kannes sich um ein bPK für den behördlichen als auch den privatwirtschaftlichen Bereich (wbPK) handeln.
    /// Für die Berechnung eines bPK wird im behördlichen Bereich das Bereichskürzel des jeweiligen staatliche
    /// Tätigkeitsbereichsgemäß Bereichsabgrenzungsverordnung [BerAbgrV] herangezogen.
    /// Im Portalverbund der österreichischen Behörden KANN die bPK für den Bereich Personalverwaltung 
    /// (Bereichskürzel PV) für Personal des Bundes verwendet werden.<br/>
    /// Für die Berechnung eines bPK des privatwirtschaftlichen Bereichs (wbPK) wird die jeweilige Stammzahl des
    /// Auftraggebers (z.B. Firmenbuchnummer) herangezogen.
    /// </summary>
    public const string BPK = "X-PVP-BPK";

    /// <summary>
    /// Fully-Qualified Host Name (FQHN) inklusive Portangabe, sofern nicht der protokollspezifische Default-Port
    /// verwendet wurde.
    /// </summary>
    public const string ORIGHOST = "X-PVP-Orig-Host";

    /// <summary>
    /// Protokollschema (http oder https) der URL, die vom Client für den Zugriff auf die angefragte Ressource verwendet
    /// hat.
    /// </summary>
    public const string ORIGSCHEME = "X-PVP-Orig-Scheme";

    /// <summary>
    /// Basierend auf dem Xff Header wird vom Standardportal der Client, der den Request absetzt,
    /// in X-ORIG-CLIENT gesetzt.
    /// </summary>
    /// <example>pc2378570.adv.magwien.gv.at</example>
    public const string ORIGCLIENT = "X-ORIG-CLIENT";

    /// <summary>
    /// Name der Applikation im Portal
    /// </summary>
    /// <example>example xx</example>
    public const string ORIGAPPLICATION = "x-portal-application";

    /// <summary>
    /// Sicherheitsklasse<br/>
    /// <seealso href="https://www.intern.magwien.gv.at/web/guest/startseite?lrurl=/mnt/nfs/wien-intern-content/wien-intern-gondor/nur-ma14/eg1/dokumentation/gondor/security-level.html"/>
    /// </summary>
    public const string SECCLASS = "X-PVP-SECCLASS";
}
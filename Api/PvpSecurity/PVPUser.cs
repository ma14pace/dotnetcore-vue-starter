﻿using System.Text.RegularExpressions;

namespace Api.PvpSecurity;

/// <summary>
/// Eingeloggter Benutzer via. PVP
/// </summary>
public record PVPUser
{
    /// <summary>
    /// Domäne
    /// </summary>
    public string Domain { get; set; }

    /// <summary>
    /// Benutzername
    /// </summary>
    public string User { get; set; }

    /// <summary>
    /// Benutzer anhand PVP-Headerwert bestimmen
    /// </summary>
    /// <remarks>Beispiele: dyn.dsstest@wien.gv.at, [citizen], 1699508@usp.gv.at</remarks>
    public static PVPUser GetDomainUser(string user)
    {
        if (!string.IsNullOrEmpty(user))
        {
            Regex regExMailsyntax = new (@"^(.*?)(?:@.*?)?$");
            var match1 = regExMailsyntax.Match(user);
            if (!match1.Success)
                return new PVPUser { User = user };
            var userNameOhneMaildomain = match1.Groups[1].Value;

            Regex regExDomainformat = new (@"(.+?)\.(.+)");
            var match = regExDomainformat.Match(userNameOhneMaildomain);
            return match.Success
                ? new PVPUser { Domain = match.Groups[1].Value, User = match.Groups[2].Value }
                : new PVPUser { User = userNameOhneMaildomain };
        }
        return null;
    }

    /// <inheritdoc />
    public override string ToString()
    {
        return $"{Domain}\\{User}";
    }
}

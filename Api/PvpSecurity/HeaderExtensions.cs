﻿namespace Api.PvpSecurity;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Web;


/// <summary>
/// Methoden für Header
/// </summary>
public static class HeaderExtensions
{
    /// <summary>
    /// Name des Fake-Header-Cookies
    /// </summary>
    public const string FAKE_HEADER_COOKIE_NAME = "fakeHeaders";

    /// <summary>
    /// Gibt einen Headerwert zurück oder null wenn nicht vorhanden
    /// </summary>
    /// <remarks>Auf Dev--Environments kann ein Cookie namens "fakeHeaders" verwendet werden
    /// um dort mit JSON-Syntax Header-Werte vorzutäuschen und zu überschreiben.</remarks>
    /// <exception cref="ArgumentNullException"/>
    public static string GetSingleHeaderValue(this HttpRequest request, string headerName)
    {
        if (request == null)
            throw new ArgumentNullException(nameof(request));

        // Fake-Header aus Cookie
        var env = request.HttpContext.RequestServices.GetService<IHostEnvironment>();
        if (env.IsDevelopment() && request.Cookies.TryGetValue(FAKE_HEADER_COOKIE_NAME, out var fakeCookie))
        {
            var fakeHeaderElement = JsonDocument.Parse(HttpUtility.UrlDecode(fakeCookie)).RootElement.EnumerateObject()
                .SingleOrDefault(prop => prop.Name.Equals(headerName, StringComparison.OrdinalIgnoreCase));
            if (!fakeHeaderElement.Equals(default))
                return fakeHeaderElement.Value.GetString();
        }

        return request.Headers?[headerName];
    }

    /// <summary>
    /// Auf Dev-Environments kann ein Cookie namens "fakeHeaders" verwendet werden um dort mit JSON-Syntax
    /// Header-Werte vorzutäuschen und zu überschreiben. Diese Methode ergänzt diese Fake-Header in die Collection.
    /// </summary>
    /// <exception cref="ArgumentNullException"/>
    public static void AddFakeHeaders(this IHeaderDictionary headers, HttpRequest request)
    {
        if (request == null)
            throw new ArgumentNullException(nameof(request));
        if (headers == null)
            throw new ArgumentNullException(nameof(headers));

        var env = request.HttpContext.RequestServices.GetService<IHostEnvironment>();
        if (env.IsDevelopment() && request.Cookies.TryGetValue(FAKE_HEADER_COOKIE_NAME, out var fakeCookie))
        {
            var jsonDoc = JsonDocument.Parse(HttpUtility.UrlDecode(fakeCookie));
            foreach (var prop in jsonDoc.RootElement.EnumerateObject())
            {
                headers[prop.Name] = prop.Value.GetString();
            }
        }
    }


    /// <summary>
    /// Eingeloggten Benutzer anhand PVP-Header bestimmen
    /// </summary>
    /// <exception cref="ArgumentNullException"/>
    public static PVPUser GetDomainUserFromHeader(this IHeaderDictionary headers)
    {
        if (headers == null)
            throw new ArgumentNullException(nameof(headers));

        string user = headers[PvpHeaders.USERID];
        return PVPUser.GetDomainUser(user);
    }

    /// <summary>
    /// Bpk aus PVP-Header bekommen
    /// </summary>
    /// <param name="headers">Request.Headers</param>
    /// <param name="trimPrefix">Entfernt Bereichszusatz wenn True (Standard)</param>
    /// <exception cref="ArgumentNullException"/>
    public static string GetPbkFromHeader(this IHeaderDictionary headers, bool trimPrefix = true)
    {
        if (headers == null)
            throw new ArgumentNullException(nameof(headers));

        string bpk = headers[PvpHeaders.BPK];
        if (!string.IsNullOrEmpty(bpk))
        {
            if (trimPrefix)
                bpk = Regex.Replace(bpk, @"^[A-Z]{2}\:", string.Empty);
            return bpk;
        }
        return null;
    }

    /// <summary>
    /// Rollen aus PVP-Header bekommen
    /// </summary>
    /// <remarks>
    /// Rollen durch ; getrennt. Beispiele:<br/>
    /// TESTROLLE<br/>
    /// ROLLE1();ROLLE2<br/>
    /// TESTROLLE(PARAM1=WERT1,PARAM2=WERT2)<br/>
    /// TESTROLLE(PARAM=ESCAPE\,WERT)<br/>
    /// </remarks>
    /// <param name="headers">Request.Headers</param>
    /// <exception cref="ArgumentNullException"/>
    public static IReadOnlyCollection<PVPRole> GetRolesFromHeader(this IHeaderDictionary headers)
    {
        if (headers == null)
            throw new ArgumentNullException(nameof(headers));

        string roles = headers[PvpHeaders.ROLES];
        if (string.IsNullOrWhiteSpace(roles))
            return null;

        var rollen = new List<PVPRoleBuilder>();
        var builder = new StringBuilder();
        RoleBuilderStatus builderStatus = RoleBuilderStatus.InRoleName;
        bool isInEscape = false;
        PVPRoleBuilder currentRole = null;
        string currentParameterName = null;
        foreach (var c in roles)
        {
            NextChar(c);
        }
        NextChar(null);

        return new ReadOnlyCollection<PVPRole>(rollen.Select(rolle => rolle.BuildPVPRole()).ToArray());

        void NextChar(char? c)
        {
            switch (builderStatus)
            {
                case RoleBuilderStatus.InRoleName:
                    if (c == ';' || c == null)
                    {
                        // Rolle ganz beenden ohne Parameter
                        rollen.Add(new PVPRoleBuilder { RoleName = builder.ToString().Trim() });
                        builder.Clear();
                    }
                    else if (c == '(')
                    {
                        // Rolle beenden, ParameterName starten
                        currentRole = new PVPRoleBuilder { RoleName = builder.ToString().Trim() };
                        builder.Clear();
                        builderStatus = RoleBuilderStatus.InParameterName;
                    }
                    else
                        builder.Append(c);
                    break;
                case RoleBuilderStatus.InParameterName:
                    if (c == null)
                        throw new InvalidCastException("Unerwartetes Ende von ParameterName.");
                    else if (c == '=')
                    {
                        // ParameterName beenden, ParameterValue starten
                        currentParameterName = builder.ToString().Trim();
                        builder.Clear();
                        builderStatus = RoleBuilderStatus.InParameterValue;
                    }
                    else if (c == ')')
                    {
                        // Keine/ungültige Parameter
                        builderStatus = RoleBuilderStatus.BeforeNextRole;
                        rollen.Add(currentRole);
                    }
                    else
                        builder.Append(c);
                    break;
                case RoleBuilderStatus.InParameterValue:
                    if (c == null)
                        throw new InvalidCastException("Unerwartetes Ende von ParameterValue.");
                    else if (isInEscape)
                    {
                        builder.Append(c);
                        isInEscape = false;
                    }
                    else if (c == '\\')
                        isInEscape = true;
                    else if (c == ',')
                    {
                        // ParameterValue beenden, nächstes ParameterName starten
                        var parameterValue = builder.ToString();
                        builder.Clear();
                        currentRole.Parameters.Add(currentParameterName, parameterValue);
                        builderStatus = RoleBuilderStatus.InParameterName;
                    }
                    else if (c == ')')
                    {
                        // ParameterValue beenden, zu nächsten Rolle
                        var parameterValue = builder.ToString();
                        builder.Clear();
                        currentRole.Parameters.Add(currentParameterName, parameterValue);
                        rollen.Add(currentRole);
                        builderStatus = RoleBuilderStatus.BeforeNextRole;
                    }
                    else
                        builder.Append(c);
                    break;
                case RoleBuilderStatus.BeforeNextRole:
                    if (c == null || c == ';')
                        builderStatus = RoleBuilderStatus.InRoleName;
                    break;
            }
        }
    }

    /// <summary>
    /// Überprüft anhand X-ORIG-CLIENT-Header ob Zugriff von außerhalb des Magistratsnetzes kommt
    /// </summary>
    /// <exception cref="ArgumentNullException"/>
    public static bool IsExternalRequest(this IHeaderDictionary headers)
    {
        if (headers == null)
            throw new ArgumentNullException(nameof(headers));

        string origin = headers[PvpHeaders.ORIGCLIENT];
        if (string.IsNullOrWhiteSpace(origin))
            return true;
        return !Regex.IsMatch(origin, @".*\.wien.gv.at$|.*\.magwien.gv.at$");
    }

    /// <summary>
    /// Überprüft anhand X-PVP-SECCLASS-Header Sicherheitsklasse<br/>
    /// <seealso href="https://www.intern.magwien.gv.at/web/guest/startseite?lrurl=/mnt/nfs/wien-intern-content/wien-intern-gondor/nur-ma14/eg1/dokumentation/gondor/security-level.html"/>
    /// </summary>
    /// <exception cref="ArgumentNullException"/>
    public static int GetSecClass(this IHeaderDictionary headers)
    {
        if (headers == null)
            throw new ArgumentNullException(nameof(headers));

        string secClass = headers[PvpHeaders.SECCLASS];
        if (string.IsNullOrWhiteSpace(secClass) || !int.TryParse(secClass, out int result))
            return 0;
        return result;
    }

    /// <summary>
    /// Gibt höchste potentiell mögliche Sicherheitsklasse anhand Header des Requests zurück (nicht tatsächliche).
    /// Dies ist von extern 2 und magistratsintern 3.<br/>
    /// <seealso href="https://www.intern.magwien.gv.at/web/guest/startseite?lrurl=/mnt/nfs/wien-intern-content/wien-intern-gondor/nur-ma14/eg1/dokumentation/gondor/security-level.html"/>
    /// </summary>
    /// <exception cref="ArgumentNullException"/>
    public static int GetHighestSecClass(this IHeaderDictionary headers)
    {
        return headers.IsExternalRequest() ? 2 : 3;
    }

    /// <summary>
    /// Überprüft anhand Header, ob Request die höchste mögliche Sicherheitsklasse aufweist.<br/>
    /// <seealso href="https://www.intern.magwien.gv.at/web/guest/startseite?lrurl=/mnt/nfs/wien-intern-content/wien-intern-gondor/nur-ma14/eg1/dokumentation/gondor/security-level.html"/>
    /// </summary>
    /// <exception cref="ArgumentNullException"/>
    public static bool IsHighestSecClass(this IHeaderDictionary headers)
    {
        var secClass = headers.GetSecClass();
        return secClass >= headers.GetHighestSecClass();
    }

    enum RoleBuilderStatus
    {
        InRoleName,
        InParameterName,
        InParameterValue,
        BeforeNextRole
    }
}
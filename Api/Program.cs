using Api.AppSettings;
using Api.Data;
using Api.PvpSecurity;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using NLog;
using NLog.Web;

var logger = LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
logger.Debug("init main");

try
{
    // Create WebApplicationBulder for Services+
    var builder = WebApplication.CreateBuilder(args);

    // Add Controller to the container.
    builder.Services.AddControllers();

    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen( c=> {
        var filePath = Path.Combine(AppContext.BaseDirectory, "Api.xml");
        c.IncludeXmlComments(filePath);
    });

    // NLog: Setup NLog for Dependency injection and delete all default log-provider
    builder.Logging.ClearProviders();
    builder.Host.UseNLog();

    // Add our Config object so it can be injected
    builder.Services.Configure<TestSettings>(builder.Configuration.GetSection("TestSettings"));

    // add Database-Context (EF) to DI
    builder.Services.AddDbContext<DbContextCscStarter>(options =>
     options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection")));

    // generate the WebApplication
    var app = builder.Build();

    // Configure the HTTP request pipeline.
    if (!app.Environment.IsProduction())
    {
        app.UseSwagger(c =>
        {
            // um Swagger-Call durchf�hren zu k�nnnen
            c.PreSerializeFilters.Add((swaggerDoc, httpReq) =>
            {
                var url = $"{httpReq.Scheme}://{httpReq.Host.Value}";
                if (httpReq.GetSingleHeaderValue(PvpHeaders.ORIGSCHEME) != null)
                {
                    url = $"{httpReq.GetSingleHeaderValue(PvpHeaders.ORIGSCHEME)}://{httpReq.GetSingleHeaderValue(PvpHeaders.ORIGHOST)}/{httpReq.GetSingleHeaderValue(PvpHeaders.ORIGAPPLICATION)}/api";
                }
                logger.Debug(url);
                swaggerDoc.Servers = new List<OpenApiServer> { new OpenApiServer { Url = url } };
            });
        });
        app.UseSwaggerUI();
    }

    //app.UseHttpsRedirection(); // im Docker CSC nicht wirklich notwendig, da hier so und so vorher ein Ingress (Proxy) ist und der https nur erkennt
    // Add ApiControllers functionality
    app.MapControllers();

    // Start App / Kestrel
    app.Run();
}
catch (Exception exception)
{
    // NLog: catch setup errors
    logger.Error(exception, "Stopped program because of exception");
    throw;
}
finally
{
    // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
    NLog.LogManager.Shutdown();
}
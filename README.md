# Dotnet Core  Docker Starter Project

Beispielprojekt für dotnetcore api und gui via ngnix.

## Urls
* [Container Smart CI Dokumentation](https://stp.wien.gv.at/container-smart-ci/docs/)
* [Source Code BitBucket](https://bitbucket.org/ma14pace/dotnetcore-vue-starter/src/develop/)
* [Container Smart CI Inventory](https://bitbucket.wien.gv.at/projects/PDF/repos/docker-smart-ci-inventories/browse)
* [Swagger API Test](https://stp-test.wien.gv.at/dotnetcore-vue-starter/api/swagger/index.html)
* [Graylog-QM](https://stp-qm.wien.gv.at/graylog/streams)
* [Graylog-Prod](https://stp.wien.gv.at/graylog/streams)
* [ARV-Eintrag auf Test](https://gondor.magwien.gv.at/arv-soap/arv3/#/application_tree%7CTest%7Cdotnetcore-vue-starter)

## Source-Code / Programm

Das Projekt ist mit .Net 6 entwickerlt und die Laufzeitumgebung ist docker (docker-compose).

### Settings

Settings werden wie überlich über `appsettings` gesetzt, und pro Umgebung gibt es ein eigenes Configurations-File. (`Staging`, `Production` und `Development`)

### Logging

Es wird mittels [nlog](https://nlog-project.org/) umgesetzt, und das Format in der Entwicklung ist eine normale Zeilenausgabe, in Test/Produktion ist das Format ein Json, um es besser in unsere Logging-Tool importieren zu können.
Die Ausgabe in allen Umgebungen ist auf StdOut.

Je Testinstanz und Prodinstanz werden verschiedene Graylog-Server angesprochen, und zwar

* [Graylog-QM](https://stp-qm.wien.gv.at/graylog/streams)
* [Graylog-Prod](https://stp.wien.gv.at/graylog/streams)

Nach dem Einrichten bzw. berechtigen des Streams (QM via PACE und Prod via BKS3) sieht man dort alle Einträge die mittels Logger gesendet worden sind. 

tbd ... eigener Stream in Graylog

~~Diese Einträge sind im Dashboard: [dotnetcore](https://stp-qm.wien.gv.at/graylog/dashboards/62b17679595fc8c1f08f1462) zu sehen, hab dich mal berechtigt.~~

~~Im Graylog kann man z.B. auch noch genauer filtern, siehe [Syntax](https://docs.graylog.org/docs/query-language), aber als Tipp, einfach das gewünschte Wort eingeben, das klappt zum größten Teil.~~

### Datenbankzugriff 

Der Zugriff erfolgt über [Entity Framework Core](https://docs.microsoft.com/en-us/ef/core/) mittels [Npgsql - .NET Access to PostgreSQL](https://www.npgsql.org/) Treiber auf die Testdatenbank. 

### Security

Die Security erfolgt in diesem Fall über HTTP-Header bzw. über die PVP-Header via Standardportal. 

Prinzipell sind die Endpoints public, bis auf die Änderungen von Datensätze, diese sind nur mit der Rolle/Security `catadmin` möglich.

Die jeweilige gewünschte Rolle kann auf Ebene von Controller oder auch Endpoint selbst definiert werden. Der Wert wird via Stp-Portal in dem Header `??` übertragen und mittels dem gewünschen Rolle verglichen. Bei Übereinstimmung wird der Entpoint ausgeführt, sonst ein 403er gesendet.
Falls keine Rolle gesetzt wurde, ist dieser Endpooint als public gekennzeichnet.

siehe auch [ARV-Eintrag](#ARVEintrag)

<span id="ARVEintrag"/>

## ARV-Eintrag
tbd


## Jenkins-Pipeline

Es werden für alle Docker Projekte die mit CSC erstellt werden ein generisches "JenkinsFile" verwendet, dies kann mittels Environment-Variablen konfiguuriert werden.
Verpflichtend sind folgende Werte:

* `GITREPOSITORY=https://bitbucket.org/ma14pace/dotnetcore-vue-starter.git` => Repo von wo das Projekt erzeugt wird
* `REPO_USER_ID=JenkinsBitbucket` => Jenkins User-Credential mit dem der SourceCode geholt werden können
* `REF=develop` => Branch von wo weg erzeugt wird, in Prod sollte dann getagged werden
* `ANSIBLE_LIMIT=dotnetcore-vue-starter` => Inventory-Limit, siehe Container Smart CI Inventory
* `ANSIBLE_JOBID=229` => Job-Id vom Ansible-Template, Achtung User `Jenkins` muss Execute-Zugriff auf das Projekt gesetzt haben
* `ANSIBLE_SERVER=Ansible Automation Platform PROD` => Unterscheidung welcher Ansible-Tower/Ansible Automation Hub verwendent werden soll

Url von Jenkins-Projekt: [Pipeline dotnetcore-vue-starter](https://stp-dev.wien.gv.at/jenkinsmaster/job/docker/job/dotnetcore-vue-starter/)

## Container Smart CI Inventory
Die Variablen werden nur dort definiert, und dürfen nicht direkt im Ansible-Template definiert werden. 
Auch verschlüsselte Variablen (Vault) werden dort gespeichert und abgelegt.

Im [Invertory-File](https://bitbucket.wien.gv.at/projects/PDF/repos/docker-smart-ci-inventories/browse/PACE/inventory.ini). 
Zusätzlich gibt's es pro Limit ein eigenes Group-File wo all die Variablen pro Projekt definiert sind, siehe [Variablen für das Projekt selbst](https://bitbucket.wien.gv.at/projects/PDF/repos/docker-smart-ci-inventories/browse/PACE/group_vars/dotnetcore-vue-starter.yml)

Die Variablen werden mittels [Jinja](https://en.wikipedia.org/wiki/Jinja_(template_engine)) in den J2-Files ersetzt, siehe dazu [Doku CSC](https://stp.wien.gv.at/container-smart-ci/docs/030_rules/004a_templates.html?h=j2).

Für Produktion hat der Betrieb ein eigenes Repo und kopiert sich dann das Ansible-Template auf ihre Prod-Umgebung.

## Ansible
[Login Ansible Automation Hub](https://automation.wien.gv.at/)

Template muss das Projekt `Docker Smart CI` referenzieren und folgendes Playbook `docker-app.yml` aufrufen.

Folgende Variablen müssen gesetzt sein bzw. werden vom Jenkins für Test gesetzt.

* `app_name: dotnetcore-vue-starter` =>  Name des CSC-Projekts, entspricht dem Namen des Jenkins-Projekts
* `app_version: develop` => entspricht dem Branch-Namen des Source Repo
* `docker_image_version: develop` => entspricht dem Branch-Namen des Source Repo

### Docker Images im Nexus

Die fertigen Images, die via Jenkins auf lxjenkinsprod.host.magwien.gv.at erzeugt werden, werden in unseren internen Nexus abgelegt.
Genauso werden die Configurationsfile (jene die mit `Volumnes` angegeben worden sind und eventuell env-Files) und das `Docker-Compose.yml`-File werden als Zip im Nexus abegelegt.

Zusätzlich wird auch noch ein `CURRENT_CONFIG_VERSION` von CSC erzeugt, dort sind alle Infos über den Branch/Commit usw. enthalten, dies kann man mit envfile eingebunden und somit aus den Environment-Variablen ausgelesen werden => siehe [}[VersionsController](https://stp-test.wien.gv.at/dotnetcore-vue-starter/api/version)

```
    env_file:
      - ./CURRENT_CONFIG_VERSION
```

Beispiel anhand dieses Projekt für Branch: `develop`
* [Api Image-File](https://lxnexus1.host.magwien.gv.at:7043/#browse/search=keyword%3Ddotnetcore:88bfafa960bab5213d7c4905968cbe52:5521c153b1b65ad958e4ae996ee64d13)
* [Gui Config-Files](https://lxnexus1.host.magwien.gv.at:7043/#browse/search=keyword%3Ddotnetcore:a1f3edd8e1b06cfa8a0ff8a3525b11f3:bad0354584a7ca1523737b6c2295c68c)
* [Config-Files](https://lxnexus1.host.magwien.gv.at:7043/#browse/search=keyword%3Ddotnetcore:2b06d2fa2e1b42a434fe2a25693b657e:8f28198dae7c72ab469065fe37a6a46d)

### Logging / Monitoring

Logging erfolgt mittels [nlog](https://nlog-project.org/), die Ausgabe erfolgt überall auf StdOut bzw. Console.
Im Docker erfolgt die Ausgabe auch auf die Console, jedoch als "einzeiliges" Json.
Diese Format kann vom installierten Filebeat, der auf jedem Dockerserver vorinstalliert ist, leicht ausgelesen und ins Graylog gesendet werden.
